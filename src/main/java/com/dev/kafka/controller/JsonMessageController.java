package com.dev.kafka.controller;

import com.dev.kafka.payload.User;
import com.dev.kafka.service.JsonKafkaProducer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/kafka")
public class JsonMessageController {
    private final JsonKafkaProducer jsonKafkaProducer;

    public JsonMessageController(JsonKafkaProducer jsonKafkaProducer) {
        this.jsonKafkaProducer = jsonKafkaProducer;
    }

    @PostMapping("/publish")
    public ResponseEntity<String> publish(@RequestBody User user) {
        this.jsonKafkaProducer.sendMessage(user);
        return ResponseEntity.ok("Json message sent to Kafka topic");
    }
}
