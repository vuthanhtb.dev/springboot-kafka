package com.dev.kafka.constants;

public class KafkaConstants {
    public static final String TOPIC_NAME = "message_demo";
    public static final String TOPIC_JSON_NAME = "json_demo";
    public static final String GROUP_ID = "myGroup";
}
