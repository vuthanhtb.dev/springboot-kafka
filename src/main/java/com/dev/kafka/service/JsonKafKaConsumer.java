package com.dev.kafka.service;

import com.dev.kafka.payload.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import static com.dev.kafka.constants.KafkaConstants.*;

@Service
public class JsonKafKaConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonKafKaConsumer.class);

    @KafkaListener(topics = TOPIC_JSON_NAME, groupId = GROUP_ID)
    public void consume(User user){
        LOGGER.info(String.format("JSON message received -> %s", user.toString()));
    }
}
