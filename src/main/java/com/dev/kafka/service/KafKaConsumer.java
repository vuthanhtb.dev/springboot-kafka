package com.dev.kafka.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import static com.dev.kafka.constants.KafkaConstants.GROUP_ID;
import static com.dev.kafka.constants.KafkaConstants.TOPIC_NAME;

@Service
public class KafKaConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafKaConsumer.class);

    @KafkaListener(topics = TOPIC_NAME, groupId = GROUP_ID)
    public void consume(String message){
        LOGGER.info(String.format("Message received -> %s", message));
    }
}
