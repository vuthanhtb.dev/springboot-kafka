package com.dev.kafka.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

import static com.dev.kafka.constants.KafkaConstants.TOPIC_JSON_NAME;
import static com.dev.kafka.constants.KafkaConstants.TOPIC_NAME;

@Configuration
public class KafkaTopicConfig {
    @Bean
    public NewTopic newTopic() {
        return TopicBuilder.name(TOPIC_NAME).build();
    }

    @Bean
    public NewTopic newJsonTopic() {
        return TopicBuilder.name(TOPIC_JSON_NAME).build();
    }
}
